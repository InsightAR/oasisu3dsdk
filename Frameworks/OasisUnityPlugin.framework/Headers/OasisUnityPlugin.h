//
//  OasisUnityPlugin.h
//  OasisUnityPlugin
//
//  Created by Carmine on 2020/4/1.
//  Copyright © 2020 Carmine. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for OasisUnityPlugin.
FOUNDATION_EXPORT double OasisUnityPluginVersionNumber;

//! Project version string for OasisUnityPlugin.
FOUNDATION_EXPORT const unsigned char OasisUnityPluginVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OasisUnityPlugin/PublicHeader.h>

#import <UIKit/UIKit.h>
#import <QCloudRealTime/QCloudAudioRecorderDataSource.h>
#import <QCloudFileRecognizer/QCloudFlashFileRecognizeParams.h>
#import <QCloudFileRecognizer/QCloudFlashFileRecognizer.h>
#import "UnityPluginAPI.h"
#import "ARUUnityMessager.h"
#import "IARCaptureRecord.h"
#import "AROasisUnityLoadEvent.h"
